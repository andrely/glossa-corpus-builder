require "rubygems"
require "sequel"

require_relative 'logging_mixin'

# Contains all the database functionality, ie. constructing the schema
# and inserting data
class CorpusDatabaseBuilder

  include Logging

  def initialize(opts={})
    @path = opts[:path] || nil
    @corpus_name = opts[:corpusname] || nil

    @db_fn = File.join(@path, "#{@corpus_name}.db")
    @tables = {}
    @db = nil

    logger.info("CorpusDatabaseBuilder initialized with path #{@path}, corpus name #{@corpus_name}, database file #{@db_fn}")
  end

  def create_db
    # using sqlite for now
    logger.info("CorpusDatabaseBuilder creating database #{@db_fn}")
    @db = Sequel.sqlite(@db_fn)
  end

  # creates the schema for the CORPUStext table from the metadata structure
  def create_schema_for_metadata(corpusname, metadata)
    columns = metadata.keys
    table = (corpusname.upcase + 'text').to_sym
    @tables[:metadata] = table

    logger.info("Creating table #{table.to_s} for metadata")

    @db.create_table(table) do
      String :tid, :default => '', :primary_key => true
    end

    @db.alter_table(table) do
      columns.each  do |col|
        add_column(col, String, {:default => '', :index => true})
        add_index(col)
      end
    end
  end

  # creates the schema for the CORPUSs_align table for alignments
  # in multilingual corpora
  def create_schema_for_alignmnents(corpusname, alignments)
    table = (corpusname.upcase + 's_align').to_sym
    @tables[:alignments] = table

    logger.info("Creating table #{table.to_s} for alignments")

    @db.create_table(table) do
      String :source, :default => '', :null => false
      String :target, :default => '', :null => false
      String :lang, :default => '', :null => false
      primary_key [:source, :target, :lang]
    end
  end

  # inserts all metadata
  def insert_metadata(metadata)
    # sort out all the tids for each metadata field
    tids = (metadata.values.collect {|v| v.keys}).flatten.sort.uniq

    cols = metadata.keys
    ds = @db[@tables[:metadata]]

    logger.info("Inserting metadata")

    tids.each do |tid|
      # insert the tid and then update with each metadata field in turn
      ds.insert(:tid => tid)

      cols.each do |col|
        val = metadata[col][tid]

        if val
          ds.where(:tid => tid).update(col => val)
        end
      end
    end
  end

  # inserts all alignments
  def insert_alignments(alignments)
    ds = @db[@tables[:alignments]]

    logger.info("Inserting alignments")

    alignments.each do |source, target, lang|
      ds.insert(:source => source, :target => target, :lang => lang)
    end
  end

  def clean
    logger.info("Clean: removing metadata database file")
    if File.exists?(@db_fn)
      logger.debug("Clean: Deleting #{@db_fn}")
      File.delete(@db_fn)
    end
  end
end