require_relative 'logging_mixin'

class TextItemBase
  include Logging

  def initialize(config, opts={})
    @config = config
    @path = opts[:path] || nil
    @relative_fn = opts[:relative_fn] || nil
    @full_fn = opts[:full_fn] || nil

    if @full_fn.nil?
      raise RuntimeError if @path.nil? or @relative_fn.nil?
      @full_fn = File.join(@path, @relative_fn)
    end

    if @path and @relative_fn.nil?
      raise RuntimeError if @full_fn.nil?
      @relative_fn = @full_fn.sub(@path, '')
    end

    @id = @relative_fn.split(File::SEPARATOR).last
    @relative_path = File.join(@relative_fn.split(File::SEPARATOR)[0...-1])
    Logging.logger.info("Added text item id #{@id} from #{@full_fn}")
  end
end