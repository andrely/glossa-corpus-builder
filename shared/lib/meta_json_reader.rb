require "json"
require "logger"

$logger = Logger.new(STDERR)

# Processor reading metadata from JSON files matching the given patterns
# with the tid extracted from the path
class MetaJSONReader
  def initialize(opts = {})
    @path = opts[:path] || nil
    @tid_list = opts[:tid_list] || nil
    @patterns = opts[:patterns] || nil

    @regexes = @patterns.collect {|p| Regexp.new(p[:filenames])}

    $logger.info("Reading JSON metadata using patterns #{@patterns.collect{|p| p[:filenames]}.join(", ")}")
  end

  def process
    metadata = {}

    return process_directory(@path, metadata)
  end

  def process_directory(dir, metadata)
    Dir.glob(File.join(dir, '*')).each do |fn|
      if File.directory?(fn)
        metadata = metadata.merge(process_directory(fn, metadata))
      else
        @regexes.each do |re|
          m = re.match(fn)

          if m
            tid = $1

            $logger.info("Reading JSON metadata for #{tid} from #{fn}")

            if @tid_list.keys.detect(tid)
              data = JSON.parse(File.open(fn, 'r').read, {:symbolize_names => true})

              data.each do |k, v|
                metadata[k] = {} if not metadata.has_key?(k)
                metadata[k][tid] = v
              end
            end
          end
        end
      end
    end

    return metadata
  end
end