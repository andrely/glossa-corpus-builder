require 'logger'

$logger = Logger.new(STDERR)

# processor extracting a metadata fields from the file name using regex patterns
class MetaFileNameReader
  def initialize(opts = {})
    @path = opts[:path] || nil
    @patterns = opts[:patterns] || nil
    @tid_list = opts[:tid_list] || nil
  end

  def process
    metadata = {}

    @patterns.each do |field, pattern|
      $logger.info("Extracting metadata from #{@path} using #{pattern}")

      re = Regexp.new(pattern)
      fields = {}

      Dir.glob(File.join(@path, '*')).each do |fn|
        short_fn = fn.sub(@path, '')
        tid = @tid_list[short_fn]

        if tid
          m = re.match(fn)
          fields[tid] = $1
        end

        metadata[field] = fields
      end

      $logger.info("Read #{fields.count} metadata entries")
    end

    return metadata
  end
end