require "logger"

class ObtConverter
  attr_accessor :filenames
  def initialize(options = {})
    @filenames = options[:tag_files].map { |pattern| Dir.glob(File.join(options[:path], pattern))}.flatten
    @columns = options[:columns] # Column field names for the tab-separated fields
    @tag_map = options[:tag_map] # OBT tags -> column field names mapping
    @documents = Array.new
  end

  def process
    parse_files()
    puts to_tabs()
  end

  def parse_files
    @filenames.each do |fn|
      @documents.push(Document.new(fn))
    end
  end

  # Creates a String containing the tab-separated output generated from the OBT+Stat input
  def to_tabs
    @documents.map { |d|
      [
          "<text id\"#{d.id}\">",
          d.sentences.map { |s|
            [
                "<s id\"#{s.id}.#{s.nr}\">",
                s.words.map { |w|
                  [w.token, w.lemma, convert_attributes(w.attributes)].flatten.join("\t")
                }.join("\n"),
                "</s>"
            ].join("\n")
          }.join("\n"),
          "</text>"
      ].join("\n")
    }.join("\n")
  end

  # Converts attributes from the input based on the @tag_map mapping
  def convert_attributes(attributes)
    converted = [*1..10].map { |e| ''}
    attributes.map {|a|
      if @tag_map.has_key?(a)
        converted[@columns.index(@tag_map[a])] = a
      end
    }
    return converted
  end
end

class Document
  attr_accessor :fn, :id, :sentences
  def initialize(fn)
    @fn = fn
    @id = extract_corpus_code(fn)
    @sentences = generate_sentences()
  end

  def extract_corpus_code(fn)
    return File.basename(fn).gsub(/\.txt\.okl$/, '')
  end

  def generate_sentences
    corpus_code = extract_corpus_code(@fn)
    sentences = Array.new
    begin
      sentence_position = 1  # Appended to the end of <s id="..."> value

      file = File.new(@fn)
      sentence = Sentence.new(corpus_code, sentence_position)
      word = Word.new

      while (line = file.gets)

        # A blank line indicates the end of the current sentence (Deprecated)
        # TODO: Commented out for now as the '<<<' tag is safer. Remove when we are sure we don't need this.
        #
        #if line =~ /^\s*$/
        #  sentences.push(sentence)
        #  sentence_position += 1
        #  sentence = Sentence.new(corpus_code, sentence_position)
        #  next
        #end

        # This pattern indicates a new word in the current sentence
        if line =~ /^"<(.*)>"/
          word.token = line.match(/^"<(.*)>"/)[1]
        end

        # This pattern indicates attributes for the current token
        if line =~ /^\t"/
          eos = /<<</.match(line) # OBT+Stat outputs a '<<<' tag upon closing a sentence
          line.strip!
          word.lemma = line.match(/^"(.+)"/)[1]
          line.sub!(/^".+"/, '').strip!
          attributes = line.strip.split(/\s+/)
          word.attributes = attributes
          sentence.add_word(word)
          word = Word.new

          # If a '<<<' have been encountered in the current attribute set,
          # Store the current sentence object and start on a new one.
          if eos
            sentences.push(sentence)
            sentence_position += 1
            sentence = Sentence.new(corpus_code, sentence_position)
          end
        end
      end
    ensure
      file.close
    end
    return sentences
  end

  def add_sentence(sentence)
    @sentences.push(sentence)
  end
end

class Sentence
  attr_accessor :id, :nr, :words
  def initialize(id, nr)
    @id = id
    @nr = nr
    @words = Array.new
  end

  def add_word(word)
    @words.push(word)
  end
end

# TODO: Might be deprecated unless we really need a 'Word' class for something.
#       If not, replace with a simpler structure.
class Word
  attr_accessor :token, :lemma, :attributes
end

def run_obt_converter_on_file(input_fn, columns, mapping)
  converter = ObtConverter.new({:tag_files => [],
                                :columns => columns,
                                :tag_map => mapping
                               })
  converter.filenames = [input_fn]
  converter.parse_files
  return converter.to_tabs
end