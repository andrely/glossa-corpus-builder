require "logger"

class OBTConverter
  def initialize(options = {})
    @filenames = filenames
    @documents = Array.new
  end

  def parse_files
    @filenames.each do |fn|
      @documents.push(Document.new(fn))
    end
  end

  def to_tabs
    return @documents.map { |d| d.to_tabs }.join("\n")
  end

  class Document
    def initialize(fn)
      @fn = fn
      @sentences = generate_sentences()
    end

    def extract_corpus_code(fn)
      return File.basename(fn).gsub(/\.txt\.okl$/, '')
    end

    def generate_sentences
      corpus_code = extract_corpus_code(@fn)
      sentences = Array.new
      begin
        file = File.new(@fn, "r:ISO-8859-1") # TODO Change to UTF8
        sentence_position = 1
        sentence = Document::Sentence.new(corpus_code, sentence_position)
        word = Document::Sentence::Word.new
        while (line = file.gets)
          # A blank line indicates the end of the current sentence
          if line =~ /^\s*$/
            sentences.push(sentence)
            sentence_position += 1
            sentence = Document::Sentence.new(corpus_code, sentence_position)
            next
          end

          # This pattern indicates a new word in the current sentence
          if line =~ /^\"<(.*)>\"/
            word.token = line.match(/^\"<(.*)>\"/)[1]
          end

          # This pattern indicates attributes for the previous token
          # encountered
          if line =~ /^\t\"/
            line.strip!
            word.lemma = line.match(/^\"(.+)\"/)[1]
            line.sub!(/^\".+\"/, '').strip!
            attributes = line.strip.split(/\s+/)
            word.attributes = attributes
            sentence.add_word(word)
            word = Document::Sentence::Word.new
          end
        end
      ensure
        file.close
      end
      return sentences
    end

    def add_sentence(sentence)
      @sentences.push(sentence)
    end

    def to_tabs
      str = "<text id=\"#{@id}\">\n" +
          @sentences.map { |s| s.to_tabs }.join("\n") +
          "\n</text>"
      return str
    end

    class Sentence
      def initialize(corpus_code, nr)
        @corpus_code = corpus_code
        @nr = nr
        @words = Array.new
      end

#      def generate_sentence_id(nr)
#        return
#      end

      def add_word(word)
        @words.push(word)
      end

      def to_tabs
        str = "<s id=\"#{@corpus_code}.#{@nr}\">\n" +
            @words.map{ |w| w.to_s }.join("\n") +
            "\n</s>"
        return str
      end

      class Word
        attr_accessor :token, :lemma, :attributes
        def to_s
          return "#{@token}\t#{@lemma}\t#{@attributes.join("\t")}"
        end
      end
    end
  end
end
