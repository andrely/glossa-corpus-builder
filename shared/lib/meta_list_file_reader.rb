require "logger"

$logger = Logger.new(STDERR)

# processor reading metadata from a file where each line contains the tid
# and metadata value.
class MetaListFileReader
  def initialize(opts = {})
    @path = opts[:path] || nil
    @files = opts[:files] || nil
    @tid_list = opts[:tid_list] || nil

    $logger.info("Reading metadata from #{@files} in #{@path}")
  end

  def process
    metadata = {}

    @files.each do |field, fn|
      entries = {}
      fn = File.join(@path, fn)

      File.open(fn, 'r').each do |line|
        line.strip!

        id, cat = line.split

        if not @tid_list.find(id)
          log.info("Meta data read for non-existent tid #{id}")
        end

        entries[id] = cat
      end

      $logger.info("Read #{entries.count} metadata entries from #{fn}")

      metadata[field] = entries
    end

    return metadata
  end
end
