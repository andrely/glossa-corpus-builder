require_relative 'logging_mixin'

##
# Corpus file list extractor selecting files using a glob
class TextGlobber
  def initialize(opts = {})
    @path = opts[:path] || nil
    @pattern = opts[:pattern] || nil

    Logging.logger.info("TextGlobber retrieving files in #{@path} using pattern #{@pattern}")
  end

  def process
    fns = Dir.glob(File.join(@path, @pattern))

    if fns.count == 0
      Logging.logger.warn("No files found")
    else
      Logging.logger.info("#{fns.count} files found")
    end

    fns.collect {|fn| File.absolute_path(fn)}
  end
end