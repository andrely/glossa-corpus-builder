require_relative 'logging_mixin'

# TODO all of this is slightly inelegant
class ConfigGenerator
  include Logging

  def initialize(config)
    @config = config

    @dbinfo = {}
    @dbinfo[:pwd] = @config[:glossa][:db_pwd]
    @dbinfo[:name] = @config[:glossa][:db_name]
    @dbinfo[:uname] = @config[:glossa][:db_uname]
    @dbinfo[:host] = @config[:glossa][:db_host]

    @urlroot = @config[:glossa][:urlroot]

    @logfile = @config[:glossa][:logfile]

    @cwb_registry = @config[:glossa][:cwb_registry]
    @config_dir = @config[:glossa][:config_dir]
    @tmp_dir = @config[:glossa][:tmp_dir]
    @dat_dir = @config[:glossa][:dat_dir]

    @corpus_name = @config[:corpusname]
    @charset = @config[:charset]
    @type = @config[:type]

    @corpus_attributes = ["word"] + @config[:cwb][:p_attributes]
    # TODO extract structure tags in a better way
    @corpus_structures = @config[:cwb][:s_attributes].collect { |str| str.gsub(/\:0\+/, '_')}
    @lang = @config[:lang]
    # TODO extract metadata fields in a better way, maybe by quering the metadata extractors directly
    #   or building the config info in the extract metadata task
    @meta_text = @config[:metadata].collect { |node| node[:values].values.collect { |v| v.keys }}.flatten

    logger.info("ConfigGenerator initialized")
  end

  def process
    logger.info("ConfigGenerator writing cgi.conf")

    File.open('cgi.conf', 'w') do |w|
      # TODO slightly inelegant
      w.write("db_pwd = #{@dbinfo[:pwd]}\n")
      w.write("db_name = #{@dbinfo[:name]}\n")
      w.write("db_uname = #{@dbinfo[:uname]}\n")
      w.write("db_host = #{@dbinfo[:host]}\n")
      w.write("charset = #{@charset}\n")
      # TODO generate urls in a more robust way
      w.write("htmlroot = #{@urlroot}/glossa/\n")
      w.write("cgiroot = #{@urlroot}/cgi-bin/glossa/\n")
      w.write("type = #{@type}\n")
      w.write("logfile = #{@logfile}\n")
      w.write("cwb_registry = #{@cwb_registry}\n")
      w.write("corpus_attributes = #{@corpus_attributes}\n")
      w.write("corpus_structures = #{@corpus_structures}\n")
      w.write("tmp_dir = #{@tmp_dir}\n")
      w.write("dat_files = #{@dat_dir}\n")
      w.write("download_url = #{@urlroot}/glossa/download/\n")
      w.write("config_dir = #{@config_dir}\n")
      w.write("subcorp_files = #{@config_dir}/#{@corpus_name.downcase}/subcorp/\n")
      w.write("hits_files = #{@config_dir}/#{@corpus_name.downcase}/hits/\n")
      w.write("lang = #{@lang}\n")
      w.write("meta_text = #{@meta_text}\n")
    end
  end
end