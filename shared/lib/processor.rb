# Generic process invoker based on an extractor/processor node in the config.
# Each node consists of a "name" member which designates the Processor interface
# class to be invoked, and a values member which is a JSON object/Hash with parameters
#
# Some processes takes specific additional parameters which are passed as options.
def invoke_processor(node, opts = {})
  class_name = node[:name]
  parameters = node[:values]
  parameters = parameters.merge(opts)

  return Kernel.const_get(class_name).new(parameters).process()
end
