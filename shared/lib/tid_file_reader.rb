require "logger"

$logger = Logger.new(STDERR)

# processor extracting tid for each file from the filename using a regex
class TidFileReader
  def initialize(opts = {})
    @path = opts[:path] || nil
    @pattern = opts[:pattern] || nil
    @files = opts[:files] || nil
  end

  def process
    $logger.info("Finding files and tids in #{@path} using #{@pattern}")

    re = Regexp.new(@pattern)

    tids = {}

    @files.each do |fn|
      base, full = fn

      m = re.match(full)

      if m
        tid = $1

        tids[base] = tid
      end
    end

    $logger.info("Found #{tids.count} corpus files")

    return tids
  end
end
