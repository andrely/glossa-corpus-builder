require "nokogiri"


class TidTEIReader
  def initialize(opts = {})
    @path = opts[:path] || nil
    @pattern = opts[:pattern] || nil
    @tidlist = {}
  end

  def process
    $logger.info("Finding files and tids in #{@path} using #{@pattern}")
    process_directory(@path)
    return @tidlist
  end

  def process_directory(dir)
    Dir.glob(File.join(dir, '*')).each do |fn|
      if File.directory?(fn)
        process_directory(fn)
      elsif File.extname(fn).eql?(".xml")
        @tidlist[fn.sub(@path, '')] = extract_tid(fn)
      end
    end
  end

  def extract_tid(fn)
    $logger.info("Extracting TID from #{fn}")
    return get_value(Nokogiri::XML(File.open(fn)).at(@pattern))
  end

  def get_value(n)
    if n.is_a?(Nokogiri::XML::NodeSet)
      return n.map { |subn| get_value(subn) }
    end
    if n.is_a?(Nokogiri::XML::Attr)
      return n.value
    end
    if n.is_a?(Nokogiri::XML::Text)
      return n.to_s
    end
    if n.nil?
      return ""
    end
  end
end