require 'nokogiri'
require 'textlabnlp/oslo_bergen_tagger'
require_relative 'text_item_base'

class TeiTextItem < TextItemBase
  attr_accessor :metadata

  def initialize(config, opts={})
    super(config, opts)
    @metadata = extract_metadata
    @text = extract_text
  end

  def annotate

  end

  # Extracts the content of the <text> tag in a tei header
  def extract_text
    doc = Nokogiri::XML(File.open(@full_fn))
    doc.encoding = 'ISO-8859-1'
    text = doc.xpath('/xmlns:TEI/xmlns:text').first.text.to_s
    return text
  end

  def id
    return /\/([^\/]+)\.tei\Z/.match(@full_fn)[1]
  end

  def extract_metadata
    doc = Nokogiri::XML(File.open(@full_fn))
    metadata = {}
    @config[:metadata].each do |m|
      m[:values][:attributes].each_pair do |attr, xpath|
        # metadata[attr] = {} if not metadata.has_key?(attr)
        metadata[attr] = do_xpath_query(doc, xpath, m[:values][:default_namespace])
      end
    end
    return metadata
  end

  def do_xpath_query(doc, xpath, namespace)
    xpath_n = xpath.split('/').map { |x| /(\(\)|@|\A\s*\Z)/.match(x) ? x : "xmlns:#{x}"}.join('/')
    # node = doc.xpath(xpath_n, 'n' => namespace)
    node = doc.xpath(xpath_n)
    val = get_value(node)
    return val
  end

  def get_value(n)
    if n.is_a?(Nokogiri::XML::NodeSet)
      return n.map { |subn| get_value(subn) }
    end
    if n.is_a?(Nokogiri::XML::Attr)
      return n.value
    end
  if n.is_a?(Nokogiri::XML::Text)
    return n.to_s
  end
    if n.nil?
      return ''
    end
  end

  def create_vert_file(file)
    # TextlabNLP
  end
end