require 'fileutils'

require_relative "corpus_database_builder"
require_relative "processor"

require_relative 'logging_mixin'

require_relative "text_globber"
require_relative "tid_file_reader"
require_relative "meta_list_file_reader"
require_relative "meta_file_name_reader"
require_relative "meta_json_reader"
require_relative "alignment_x_path_extractor"
require_relative "tid_tei_reader"
require_relative "meta_tei_reader"
require_relative "cwb_encoder"
require_relative "config_generator"
require_relative "opc_text_item"
require_relative "tei_text_item"
require_relative "lbk_text_item"

# main class responsible for running all the tasks specified in the config file
# and constructing the corpus
class CorpusBuilder

  include Logging

  def initialize(config)
    @config = config
    @files = nil
    @metadata = nil

    @text_items = nil

    @db_builder = nil
  end

  def text_items(parallels=nil)
    if parallels
      items = []
      @text_items.each do |item|
        if item.kind_of?(OPCTextItem)
          items += item.parallels
        else
          items << item
        end
      end

      items
    else
      @text_items
    end
  end

  def init_text_items
    Logging.logger.info("Collecting text items for corpus #{@config[:corpusname]} in #{@config[:path]}")

    config = @config[:text_item]
    text_item_class = Kernel.const_get(config[:name])
    @text_items = CorpusBuilder.init_text_items_for_files(@config[:path], get_files, text_item_class, @config)
  end

  ##
  # @private
  def self.init_text_items_for_files(path, files, text_item_class, config)
    files.collect do |full_fn|
      text_item_class.new(config, path: path, full_fn: full_fn)
    end
  end

  def create_cwb_files
    out_path = annotation_path

    text_items(true).each do |item|
      item_vert_fn = item.vert_fn(out_path)
      
      FileUtils.mkpath(File.dirname(item_vert_fn)) unless File.exists?(File.dirname(item_vert_fn))
      
      File.open(item_vert_fn, 'w') do |f|
        item.create_vert_file(f)
      end
    end
  end

  def create_explicit_alignment_files
    out_path = annotation_path
    
    text_items(true).each do |item|
      item_alignment_fn = item.alignment_fn(out_path)

      FileUtils.mkpath(File.dirname(item_alignment_fn)) unless File.exists?(File.dirname(item_alignment_fn))

      File.open(item_alignment_fn, 'w') do |f|
        item.create_alignment_file(f)
      end
    end
  end

  # Creates positions table and tab file containing the whole corpus.
  #
  # @note Corpus building task.
  def register_positions_and_create_tab_files
    words_so_far = {}
    pos_fn = File.join(data_path, 'pos')

    Logging.logger.info("Writing corpus positions to #{pos_fn}")

    File.open(pos_fn, 'w') do |f|
      text_items(true).each do |item|
        lang = item.lang
        words_so_far[lang] = 0 unless words_so_far.has_key?(lang)

        cur_pos = item.register_positions(f, words_so_far[lang], annotation_path)
        words_so_far[lang] = cur_pos
      end
    end

    # tab files are appended to, so we remove any existing tab files before
    # writing new ones.
    Dir.glob(File.join(data_path, "#{corpusname}-*.tab")) do |fn|
      Logging.logger.warn("Deleting existing tab file #{fn}")
      File.delete(fn)
    end

    Logging.logger.info("Writing tab files in #{data_path}")

    text_items(true).each do  |item|
      item.write_tab_file(data_path, annotation_path)
    end
  end

  def create_alg_files
    Dir.glob(File.join(data_path, "#{corpusname}-*.alg")) do |fn|
      File.delete(fn)
    end

    pos_map = {}

    File.open(File.join(data_path, 'pos'), 'r').each do |line|
      id, start_pos, end_pos = line.split("\t")
      pos_map[id] = [start_pos.strip, end_pos.strip]
    end

    text_items(true).each do |item|
      item.write_alg_file(data_path, annotation_path, pos_map)
    end

  end

  # Creates CWB indexed corpus.
  # Runs CWB commands for encoding and compressing corpora.
  #
  # @note Corous building task.
  def cwb_encode
    # only for OPC
    # monolingual corpora should only do a single encode step
    encoder = CWBEncoder.new(config: @config[:cwb], corpusname: corpusname,
                             data_dir: data_path, cwb_dir: cwb_path)

    # create corpus for each language parallel
    lang_list.each do |lang|
      encoder.encode(parallel_id: lang)
    end

    # add alignments
    lang_list.each do |lang|
      alignments = lang_list
      alignments.delete(lang)
      encoder.encode_alignments(parallel_id: lang, alignments: alignments)
    end
  end

  # TASK
  #
  # extracts the metadata with each extractor and hand it to the database builder
  def store_metadata
    metadata = get_metadata
    db_builder = get_db_builder
    db_builder.clean
    db_builder.create_db_connection
    db_builder.create_schema_for_metadata(@config[:corpusname], metadata)
    db_builder.insert_metadata(metadata)
  end

  # TASK
  #
  def generate_config
    config_gen = ConfigGenerator.new(@config)

    config_gen.process
  end

  def get_db_builder
    if @db_builder.nil?
      @db_builder = CorpusDatabaseBuilder.new(path: @config[:path], corpusname: @config[:corpusname])
    end

    @db_builder
  end

  # corpus file list extractor using the texts node in the config.
  # The file list is an array with arrays of file paths relative to the corpus
  # location and the absolute path.
  def get_files
    if @files.nil?
      @files = []

      @config[:texts].each do |node|
        @files += invoke_processor(node, path: @config[:path])
      end
    end

    @files
  end

  def annotation_path
    tmp_path = File.join(@config[:path], 'tmp')
    FileUtils.mkpath(tmp_path) unless File.exists?(tmp_path)
    tmp_path
  end

  def data_path
    data_path = File.join(@config[:path], 'data')
    FileUtils.mkpath(data_path) unless File.exists?(data_path)
    data_path
  end

  def cwb_path
    cwb_path = File.join(@config[:path], 'cwb')
    FileUtils.mkpath(cwb_path) unless File.exists?(cwb_path)
    cwb_path
  end
  
  def lang_list
    langs = []
    text_items(true).each { |item| langs << item.lang unless langs.member?(item.lang) }

    langs
  end

  # Canonical corpus name for file names, id's etc.
  #
  # @return [String]
  def corpusname
    # always use downcase corpus names to avoid case confusion in CWB
    @config[:corpusname].downcase
  end
end
