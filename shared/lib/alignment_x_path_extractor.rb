require "nokogiri"

$logger = Logger.new(STDERR)

# processor that extracts alignments from XML files aligned by TCA2
class AlignmentXPathExtractor
  def initialize(opts = {})
    @path = opts[:path] || nil
    @xpaths = opts[:xpaths] || nil
    @tid_list = opts[:tid_list] || nil
  end

  def process
    $logger.info("Extracting alignments using #{self.class.name}")
    alignments = []

    @tid_list.each do |fn, tid|
      File.open(File.join(@path, fn), 'r') do |f|
        target_lang = fn[-7..-5]

            doc = Nokogiri::XML(f)
        count = 0

        @xpaths.each do |xpath|
          $logger.info("Reading alignments from #{fn} using xpath #{xpath}")

          doc.xpath(xpath).each do |node|
            count += 1
            source = node['id']
            targets = node['corresp'].split

            # add multiple alignments separately
            targets.each { |target| alignments << [source, target, target_lang]}
          end
        end

        $logger.info("Read #{count} alignments from #{fn}")
      end
    end

    $logger.info("#{alignments.count} alignments read by #{self.class.name}")

    return alignments
  end
end
