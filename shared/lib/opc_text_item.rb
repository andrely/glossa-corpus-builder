require 'json'
require 'tempfile'

require 'textlabnlp/oslo_bergen_tagger'
require 'textlabnlp/tree_tagger'

require_relative 'logging_mixin'
require_relative 'globals'

class OPCTextItem

  include Logging

  attr_reader :path, :relative_fn, :relative_path, :full_fn, :id, :parallels

  def initialize(config, opts={})
    @config = config
    @path = opts[:path] || nil
    @relative_fn = opts[:relative_fn] || nil
    @full_fn = opts[:full_fn] || nil

    if @full_fn.nil?
      raise RuntimeError if @path.nil? or @relative_fn.nil?
      @full_fn = File.join(@path, @relative_fn)
    end

    if @path and @relative_fn.nil?
      raise RuntimeError if @full_fn.nil?
      @relative_fn = File.absolute_path(@full_fn).sub(File.absolute_path(@path) + File::SEPARATOR, '')
    end

    @id = @relative_fn.split(File::SEPARATOR).last
    @pivot = @config[:pivot].to_sym
    @parallels = init_parallels(@id, @full_fn, @pivot)
    @relative_path = File.join(@relative_fn.split(File::SEPARATOR)[0...-1])

    Logging.logger.info("Added text item id #{@id} from #{@full_fn}")
  end

  def self.correspondences(id, pivot, full_path)
    fns = Dir.glob(File.join(full_path, "#{id}-*-*.xml"))
    fn_langs = []

    fns.each do |fn|
      corr = OPCTextItem.extract_lang(fn, id)

      if fn_langs.include?(corr)
        Logging.logger.warn("Duplicate source file for correspondence #{lang}-#{corresp_lang}")
      end

      fn_langs << corr
    end

    correspondences = []

    fn_langs.each do |lang, corresp_lang|
      unless fn_langs.include?([corresp_lang, lang])
        Logging.logger.warn("Only one side for correspondence #{lang}-#{corresp_lang} found")
        next
      end

      unless lang == pivot or corresp_lang == pivot
        Logging.logger.warn("Pivot #{pivot} not part of correspondence #{lang}-#{corresp_lang}")
        next
      end

      if lang == pivot
        corr = [lang, corresp_lang]
      else
        corr = [corresp_lang, lang]
      end

      correspondences << corr unless correspondences.include?(corr)
    end

    correspondences
  end

  def init_parallels(id, full_fn, pivot)
    corr = OPCTextItem.correspondences(id, pivot, full_fn)

    corr.collect do |lang, corresp_lang|
      metadata = extract_metadata(id, corresp_lang, full_fn)

      ParallelTextItem.new(id: "#{id}-#{lang}", config: @config, full_fn: full_fn,
                           lang: lang, corresp_lang: corresp_lang,
                           metadata: metadata, parent_item: self)
    end
  end

  def extract_metadata(id, lang, full_fn)
    metadata_fn = File.join(full_fn, "#{id}-#{lang}.meta.json")

    return Hash.new unless File.exists?(metadata_fn)

    File.open(metadata_fn, 'r') do |f|
      JSON.parse(f.read, {:symbolize_names => true})
    end
  end

  def self.extract_lang(fn, id)
    fn = File.split(fn).last
    m = fn.match("#{id}-(\\w+)-(\\w+).xml")

    raise RuntimeError if m.nil?

    m.captures.collect { |g| g.to_sym }
  end

  def self.create_vert_file
    Logging.logger.info("Creating vert file for #{@id} in #{vert_file.path}") if vert_file.kind_of?(File)
    annotator =
        case @lang
          when :nob, :nno
            TextlabNLP::OsloBergenTagger.new(config: get_tools_config[:obttagger])
          when :eng, :fra, :swe
            TextlabNLP::TreeTagger.for_lang(@lang, config: get_tools_config[:treetagger])
          else
            raise NotImplementedError
        end

    text_file = StringIO.new

    File.open(@full_fn, 'r') do |f|
      ParallelTextItem.tei_to_text(f, text_file)
    end

    text_file.rewind

    Logging.logger.info("Running #{annotator.class.to_s}")

    text =
        case @lang
          when :nob
            annotator.annotate(file: text_file, disambiguate: true, sent_seg: :xml)
          when :nno
            annotator.annotate(file: text_file, disambiguate: true, sent_seg: :xml, lang: :nn)
          when :eng, :fra, :swe
            annotator.annotate(file: text_file, sent_seg: :xml)
          else
            raise NotImplementedError
        end

    ParallelTextItem.text_to_vert(vert_file, text)

    vert_file
  end

  def create_vert_files(out_path)
    out_path = File.join(out_path, relative_fn)

    # pivot
    Logging.logger.info("Creating vert file for #{@id} in #{vert_file.path}") if vert_file.kind_of?(File)
    pivot_tei_fn = File.join(out_path, "#{id}-#{pivot}-#{@parallels.first.corresp_lang}")
    OPCTextItem.create_vert_file
  end
end

class ParallelTextItem

  include Logging

  attr_reader :id, :full_fn, :lang, :corresp_lang, :metadata

  def initialize(opts={})
    @id = opts[:id] || nil
    @config = opts[:config] || nil
    @full_fn = opts[:full_fn] || nil
    @lang = opts[:lang] || nil
    @corresp_lang = opts[:corresp_lang] || nil
    @metadata = opts[:metadata] || nil
    @parent_item = opts[:parent_item] || nil
  end

  def vert_fn(root_path=nil)
    fn = File.join(@parent_item.relative_fn, "#{id}.vert")

    if root_path
      File.join(root_path, fn)
    else
      fn
    end
  end

  def self.tei_to_text(tei_file, out_file)
    doc = Nokogiri::XML(tei_file)

    doc.xpath("//s").each do |s|
      out_file.write("<s id=\"#{s[:id]}\">")
      out_file.write(s.text.strip) if s.text.strip != ''
      out_file.puts("</s>")
    end

    out_file
  end

  def self.text_to_vert(vert_file, text)
    vert_file.puts("<text>")

    text.each do |sent|
      vert_file.puts("<s id=\"#{sent[:id]}\">")

      sent[:words].each do |word|
        vert_file.puts("#{word[:word]}\t#{word[:annotation][0][:lemma]}\t#{word[:annotation][0][:tag]}")
      end

      vert_file.puts("</s>")
    end

    vert_file.puts("</text>")

    vert_file
  end

  def create_vert_file(vert_file)
    Logging.logger.info("Creating vert file for #{@id} in #{vert_file.path}") if vert_file.kind_of?(File)
    annotator =
        case @lang
          when :nob, :nno
            TextlabNLP::OsloBergenTagger.new(config: get_tools_config[:obttagger])
          when :eng, :fra, :swe
            TextlabNLP::TreeTagger.for_lang(@lang, config: get_tools_config[:treetagger])
          else
            raise NotImplementedError
    end

    text_file = StringIO.new

    File.open(@full_fn, 'r') do |f|
      ParallelTextItem.tei_to_text(f, text_file)
    end

    text_file.rewind

    Logging.logger.info("Running #{annotator.class.to_s}")

    text =
        case @lang
          when :nob
            annotator.annotate(file: text_file, disambiguate: true, sent_seg: :xml)
          when :nno
            annotator.annotate(file: text_file, disambiguate: true, sent_seg: :xml, lang: :nn)
          when :eng, :fra, :swe
            annotator.annotate(file: text_file, sent_seg: :xml)
          else
            raise NotImplementedError
        end

    ParallelTextItem.text_to_vert(vert_file, text)

    vert_file
  end

  def alignment_fn(root_path=nil)
    fn = File.join(@parent_item.relative_fn, "#{id}.alignment")

    if root_path
      File.join(root_path, fn)
    else
      fn
    end
  end

  def self.extract_alignments(tei_file)
    alignments = {}
    doc = Nokogiri::XML(tei_file)

    doc.xpath("//s").each do |s|
      id = s[:id]
      corresp = s[:corresp]

      Logging.logger.warn("Malformed s entry") unless id and corresp

      alignments[id.strip] = corresp.split.collect { |c| c.strip }
    end

    alignments
  end

  def create_alignment_file(alignment_file)
    Logging.logger.info("Creating alignment file for #{@id} in #{alignment_file.path}") if alignment_file.kind_of?(File)

    alignments = File.open(@full_fn, 'r') do |f|
      ParallelTextItem.extract_alignments(f)
    end

    alignments.each_pair do |id, corresp|
      corresp.sort.each do |c|
        alignment_file.puts("#{id}\t#{c}\t#{@corresp_lang.to_s}")
      end
    end

    alignment_file
  end

  # Write pos table data to file for the vert file and offset given.
  #
  # @param [IO, StringIO] pos_file Pos table data file.
  # @param [IO, StringIO] vert_file Input file.
  # @param [Fixnum] words_so_far Start offset for the position values
  # @return [Fixnum] Incremented final offset.
  def self.register_positions(pos_file, vert_file, words_so_far)
    # @todo verify correct offsets for first sentence
    position = words_so_far - 1
    id = nil
    startpos = nil
    endpos = nil

    vert_file.readlines.each do |line|
      if line =~ /<s\s+id="(.+)">/
        # Found the start of an s-unit.
        id = $1
      elsif line.include?('</s>')
        # Found the end of an s-unit
        endpos = position

        if id && startpos && endpos
          #@mysql.query("INSERT INTO #{POSTABLE} SET s_id='#{id}', " +\
          #               "startpos=#{startpos}, endpos=#{endpos}")
          pos_file.puts("#{id}\t#{startpos}\t#{endpos}")
        elsif id && endpos
          # No startpos means that nothing was found between the start and the
          # end of the s-unit - that is OK
          STDERR.puts("EMPTY S-UNIT: #{id}")
        else
          STDERR.puts("ERROR: id=#{id}, startpos=#{startpos}, endpos=#{endpos}")
        end

        id = startpos = endpos = nil
      elsif line =~ /<\/?text>/
      elsif line =~ /\S/
        # We are in the middle of an s-unit
        position += 1
        startpos ||= position
      end
    end

    position + 1
  end

  def register_positions(pos_file, words_so_far, vert_root=nil)
    fn = vert_fn(vert_root)

    Logging.logger.info("Registering positions for #{fn}")

    File.open(fn, 'r') do |f|
      ParallelTextItem.register_positions(pos_file, f, words_so_far)
    end
  end

  def tab_fn(root_path=nil)
    fn = "#{@config[:corpusname]}_#{@lang}.tab".downcase

    if root_path
      File.join(root_path, fn)
    else
      fn
    end
  end
  
  def write_tab_file(tab_root=nil, vert_root=nil)
    tab_fn = tab_fn(tab_root)
    vert_fn = vert_fn(vert_root)

    Logging.logger.info("Writing #{vert_fn} to #{tab_fn}")

    File.open(tab_fn, 'a') do |tab_f|
      File.open(vert_fn, 'r').each do |line|
        tab_f.puts(line)
      end
    end
  end

  def write_alg_file(data_path, tmp_path, pos_map)
    
    alignments = {}

    File.open(alignment_fn(tmp_path)).each do |line|
      source, target, _ = line.split("\t")
      if alignments.has_key?(source)
        alignments[source] << target
      else
        alignments[source] = [target]
      end
    end

    alg_fn = File.join(data_path, "#{@config[:corpusname]}-#{@lang}-#{@corresp_lang}.alg".downcase)

    unless File.exists?(alg_fn)
      File.open(alg_fn, 'w') do |f|
        f.puts("#{@config[:corpusname]}_#{@lang}\ts\t#{@config[:corpusname]}_#{@corresp_lang}\ts".downcase)
      end
    end

    File.open(alg_fn, 'a') do |f|
      alignments.each_pair do |source, targets|
        src_start, src_end = pos_map[source]
        target_positions = targets.collect { |t| pos_map[t] }
        target_start = target_positions.collect { |pos| pos.first }.min
        target_end = target_positions.collect { |pos| pos[1] }.max

        f.puts("#{src_start}\t#{src_end}\t#{target_start}\t#{target_end}\t1:#{targets.count}")
      end
    end

  end
end
