require_relative "tei_text_item"

class LbkTextItem < TeiTextItem

  attr_accessor :category, :topics, :authors_analytic, :authors_monogr

  def initialize(config, opts={})
    super(config, opts)
    process_metadata()
    process_authors()
  end

  def process_metadata
    process_catref()
  end

  # Splits the :catRef string into a category (the first code in the string)
  # and a set of 0 or more categories (the remainder of the string)
  def process_catref
    catRef = @metadata[:catRef][0]
    @category = catRef[0...4]
    @topics = []
    if catRef.length > 4
      topics_str = catRef[4..-1]
      topics_str.split(/\s+/).each do |t|
        @topics << t.strip unless /\A\s*\Z/.match(t)
      end
    end
  end

  def process_authors
    authors_analytic = [@metadata[:analytic_authorname],
                        @metadata[:analytic_authorgeogr],
                        @metadata[:analytic_authorsex],
                        @metadata[:analytic_authorborn]]

    authors_monogr = [@metadata[:monogr_authorname],
                      @metadata[:monogr_authorgeogr],
                      @metadata[:monogr_authorsex],
                      @metadata[:monogr_authorborn]]

    @authors_analytic = []
    @authors_monogr = []

    (0...authors_analytic[0].length).each do |i|
      @authors_analytic << {
          :name => @metadata[:analytic_authorname][i],
          :geogr => @metadata[:analytic_authorgeogr][i],
          :sex => @metadata[:analytic_authorsex][i],
          :born => @metadata[:analytic_authorborn][i]
      }
    end

    (0...authors_monogr[0].length).each do |i|
      @authors_monogr << {
          :name => @metadata[:monogr_authorname][i],
          :geogr => @metadata[:monogr_authorgeogr][i],
          :sex => @metadata[:monogr_authorsex][i],
          :born => @metadata[:monogr_authorborn][i]
      }
    end
  end

  def vert_fn(root_path=nil)
    fn = File.join(@parent_item.relative_fn, "#{id}.vert")

    if root_path
      File.join(root_path, fn)
    else
      fn
    end
  end

  def create_vert_file(file)
    tagger = TextlabNLP::OsloBergenTagger.new(config: get_tools_config[:obttagger])
    text_tmp_file = StringIO.new

    File.open(@full_fn, 'r') do |f|
      # TODO: Text in LBK TEI files have no tags whatsoever as of now

      text_tmp_file.write(extract_text)

      # doc.xpath("//s").each { |s| text_tmp_file.puts(s.text.strip) if s.text.strip != "" }
    end

    text_tmp_file.close

    out = tagger.annotate(file: StringIO.new(text_tmp_file.string), disambiguate: true)

    file.puts("<text>")

    out.each do |sent|
      file.puts("<s>")

      sent[:words].each do |word|
        file.puts("#{word[:word]}\t#{word[:annotation][0][:lemma]}\t#{word[:annotation][0][:tag]}")
      end

      file.puts("</s>")
    end

    file.puts("</text>")
  end

  # TODO Construct simple TSV files from metadata
end