require "nokogiri"
require "logger"
require "json"

$logger = Logger.new(STDERR)

class MetaTEIReader

  def initialize(opts = {})
    @path = opts[:path] || nil
    @attributes = opts[:attributes] || nil
    @tid_list = opts[:tid_list] || nil
  end

  def process
    $logger.info("Reading TEI header information")
    metadata = {}
    @tid_list.each_pair do |fn, tid|
      path = File.join(@path, fn)
      process_teiheader(path, tid, metadata)
    end
    # return process_directory(@path, {})
    return metadata
  end

  #def process_directory(dir, metadata)
  #  Dir.glob(File.join(dir, '*')).each do |fn|
  #    if File.directory?(fn)
  #      metadata = metadata.merge(process_directory(fn, metadata))
  #    elsif File.extname(fn).eql?(".xml")
  #      $logger.info("Reading TEI metadata from #{fn}")
  #      process_teiheader(fn, metadata)
  #    end
  #  end
  #  return metadata
  #end

  def process_teiheader(fn, tid, metadata)
    doc = Nokogiri::XML(File.open(fn))
    @attributes.each_pair do |attr, xpath|
      metadata[attr] = {} if not metadata.has_key?(attr)
      metadata[attr][tid] = get_value(doc.at(xpath))
    end
  end

  def get_value(n)
    if n.is_a?(Nokogiri::XML::NodeSet)
      return n.map { |subn| get_value(subn) }
    end
    if n.is_a?(Nokogiri::XML::Attr)
      return n.value
    end
    if n.is_a?(Nokogiri::XML::Text)
      return n.to_s
    end
    if n.nil?
      return ""
    end
  end
end
