require_relative "../process_corpus"

# A set of functions giving access to global configuration.

# Returns the root project path
def get_root_path()
  root = File.join(File.dirname(__FILE__), "../..")
  root = File.absolute_path(root)

  return root
end

$TOOLS_CONFIG_FN = File.join(get_root_path(), 'shared/tools_config.json')
$TOOLS_CONFIG = nil

# Returns a hash based on the tools_config json file
def get_tools_config()
  if $TOOLS_CONFIG.nil?
    $TOOLS_CONFIG = read_config_file($TOOLS_CONFIG_FN)
  end

  return $TOOLS_CONFIG
end
