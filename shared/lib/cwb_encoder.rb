require "pathname"
require 'fileutils'

require_relative 'logging_mixin'
require_relative 'globals'

# NOTE by default the CWB encoder deletes old indices and registry files
# do not point CWBEncoder to a dir outside of the corpus path to avoid
# accidental deletion

class CWBEncoder
  # @todo Most of this should move to Textlab-NLP gem.

  include Logging

  def initialize(opts={})
    @corpus_name = opts[:corpusname] || raise(ArgumentError)
    @config = opts[:config] || raise(ArgumentError)
    @data_dir = opts[:data_dir] || raise(ArgumentError)
    @cwb_dir = opts[:cwb_dir] || raise(ArgumentError)

    @cwb_data_dir = File.join(@cwb_dir, 'data')
    @cwb_reg_dir = File.join(@cwb_dir, 'reg')

    Logging.logger.info("CWBEncoder initialized corpus name #{@corpus_name}, cwb dir #{@cwb_dir}, data dir #{@data_dir}")
  end

  # @private
  # @param [String] corpusname As it will appear in filenames, registry entries, etc.
  # @param [String] data_dir CWB corpus data dir.
  # @param [String] registry Path to CWB registry.
  # @param [Array<String>] tab_files Array of paths to tab corpus files.
  # @param [Hash] attributes CWB s and p attributes in th :s and :p keys respectively.
  def self.encode_corpus(corpusname, data_dir, registry, tab_files, attributes)
    FileUtils.mkpath(data_dir) unless File.exists?(data_dir)
    FileUtils.mkpath(registry) unless File.exists?(registry)

    for fn in tab_files
      unless File.exists?(fn)
        Logging.logger.warn("Tab file #{fn} could not be found")
        tab_files.delete(fn)
      end
    end

    if tab_files.empty?
      Logging.logger.warn("No tab files")
      return
    end

    config = get_tools_config()

    args = CWBEncoder.generate_encoding_arguments(tab_files, data_dir, File.join(registry, corpusname),
                                                  attributes)
    encode_cmd = "#{config[:cwb][:cwb_encode]} #{args.join(" ")}"
    Logging.logger.info("CWBEncoder running command #{encode_cmd}")
    status = TextlabNLP.run_shell_command(encode_cmd)

    unless status.exitstatus == 0
      Logging.logger.warn("CWD encode failed")
    end

    make_cmd = "#{config[:cwb][:cwb_makeall]} -r #{registry} -V #{corpusname.downcase}"
    Logging.logger.info("Running CWB make-all with command #{make_cmd}")
    status = TextlabNLP.run_shell_command(make_cmd)

    unless status.exitstatus == 0
      Logging.logger.warn("CWB makeall failed")
    end

    huff_cmd = "#{config[:cwb][:cwb_huffcode]} -r #{registry} -A #{corpusname.downcase}"
    Logging.logger.info("Running CWB huffcode with command #{huff_cmd}")
    status = TextlabNLP.run_shell_command(huff_cmd)

    unless status.exitstatus == 0
      Logging.logger.warn("CWB huffcode failed")
    end

    # @todo compress step here
  end

  # @private
  # @param [String] registry Path to CWB registry.
  # @param [String] alg_file Path to alignment file.
  def self.encode_alignment(registry, alg_file)
    config = get_tools_config
    cmd = "#{config[:cwb][:cwb_align_encode]} -v -r #{registry} -D -C #{alg_file}"
    Logging.logger.info("Encoding alignment with command #{cmd}")
    TextlabNLP.run_shell_command(cmd)
  end

  # Create CWB indexed corpus
  #
  # @note May overwrite existing corpus.
  #
  # @option opts [Symbol] parallel_id Symbol identifying language parallel for multilingual corpora.
  def encode(opts={})
    parallel_id = opts[:parallel_id] || nil

    if parallel_id
      corpus_name = "#{@corpus_name}_#{parallel_id.to_s.downcase}"
      Logging.logger.info("Creating CWB corpus files for corpus #{@corpus_name}, parallel #{parallel_id.to_s}")
    else
      corpus_name = @corpus_name
      Logging.logger.info("Creating CWB corpus files for corpus #{@corpus_name}")
    end

    cwb_data_dir = File.join(@cwb_data_dir, corpus_name)
    tab_files = [File.join(@data_dir, "#{corpus_name}.tab")]

    CWBEncoder.encode_corpus(corpus_name, File.absolute_path(cwb_data_dir), @cwb_reg_dir, tab_files, @config[:attributes])
  end

  # Add alignments from generated alignments file to the corpus.
  #
  # @option opts [Symbol] parallel_id The corpus parallel alignments will be added to.
  # @option opts [Array<Symbol>] alignments Languages to add alignments for.
  def encode_alignments(opts={})
    parallel_id = opts[:parallel_id].to_s.downcase || raise(ArgumentError)
    alignments = opts[:alignments] || raise(ArgumentError)

    alignments.each do |alignment|
      alignment = alignment.to_s.downcase

      alg_file = File.join("#{@data_dir}", "#{@corpus_name}-#{parallel_id}-#{alignment}.alg")

      unless File.exists?(alg_file)
        Logging.logger.warn("No alg file #{alg_file} for #{parallel_id}-#{alignment} alignment")
        next
      end

      reg_file = File.join(@cwb_reg_dir, "#{@corpus_name}_#{parallel_id}")
      aligned_corpus = "#{@corpus_name}_#{alignment}"

      # add alignment attribute to registry
      Logging.logger.info("Adding alignment for #{alignment} to #{reg_file}")
      File.open(reg_file, 'a+') do |f|
        f.write("\nALIGNED #{aligned_corpus}\n")
      end

      Logging.logger.info("Encoding alignment for #{parallel_id}-#{alignment} from #{alg_file}")
      CWBEncoder.encode_alignment(@cwb_reg_dir, alg_file)
    end
  end

  # @private
  def self.generate_encoding_arguments(tab_files, data_dir, registry, attributes)
    p_attributes = attributes[:p] || []
    s_attributes = attributes[:s] || []
    arguments = ["-xsB"] + # TODO What does -x, -s and -B do?
        Array.new(tab_files.length, "-f").zip(tab_files).flatten +
        ["-d", data_dir] +
        ["-R", registry] +
        ["-c ", 'utf8'] +
        Array.new(p_attributes.length, "-P").zip(p_attributes).flatten +
        Array.new(s_attributes.length, "-S").zip(s_attributes).flatten
    return arguments
  end

  def generate_compress_rdx_arguments
    #arguments = ["-T", "-A"] +
    #    ["-r", @registry_directory] +
    #    [ @corpus_name.upcase ]
    #return arguments
  end

  def compress
    #tools_config = get_tools_config()
    #
    #huff_cmd = "#{tools_config[:cwb][:cwb_huffcode]} #{generate_huffcode_arguments.join(" ")}"
    #logger.info("CWBEncoder running command #{huff_cmd}")
    #
    #if not system(huff_cmd)
    #  logger.warn("CWBEncoder command failed")
    #end
    #
    #comp_cmd = "#{tools_config[:cwb][:cwb_compress]} #{generate_compress_rdx_arguments.join(" ")}"
    #logger.info("CWBEncoder running command #{comp_cmd}")
    #
    #if not system(comp_cmd)
    #  logger.warn("CWBEncoder command failed")
    #end
    #
    #delete_uncompressed_files
  end

  def delete_uncompressed_files
    #logger.info("CWBEncoder deleteing uncompressed data files")
    #
    #Dir.glob("#{@data_dir}/*corpus").each do |fn|
    #  logger.debug("CWBEncoder deleting #{fn}")
    #  File.delete(fn)
    #end
    #
    #Dir.glob("#{@data_dir}/*rev").each do |fn|
    #  logger.debug("CWBEncoder deleting #{fn}")
    #  File.delete(fn)
    #end
    #
    #Dir.glob("#{@data_dir}/*rdx").each do |fn|
    #  logger.debug("CWBEncoder deleting #{fn}")
    #  File.delete(fn)
    #end
  end

  def clean
    #logger.info("Clean: Removing previous CWB index and registry")
    #
    #if Dir.exist?(@registry_directory)
    #  logger.debug("Clean: Deleting #{@registry_directory} with all contents")
    #  FileUtils.rmtree(@registry_directory)
    #end
    #
    #dat_dir = @data_dir.gsub(/#{@corpus_name.upcase}\/?$/, '')
    #if Dir.exist?(dat_dir)
    #  logger.debug("Clean: Deleting #{dat_dir} with all contents")
    #  FileUtils.rmtree(dat_dir)
    #end
  end
end
