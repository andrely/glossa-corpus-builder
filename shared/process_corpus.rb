require 'json'

require_relative 'lib/tasks'
require_relative 'lib/corpus_builder'

# Reads a JSON config file with the corpus building parameters
def read_config_file(fn)
  File.open(fn, 'r') do |f|
    return JSON.parse(f.read(), {:symbolize_names => true})
  end
end

##
# Top level processor
#
# ==== Attributes
# * +tasks+ A Hash containing the task sequence. Tasks must be methods on CorpusBuilder prefixed by the sequence number.
# * +options+ Currently only +:config_fn+ to specify the config file
#
def process_corpus(tasks, opts = {})
  config_fn = File.absolute_path(opts[:config_fn]) || nil

  config = read_config_file(config_fn)

  # override config file with passed opts
  config = config.merge(opts)

  STDERR.puts('Started at ' + `date`)

  builder = CorpusBuilder.new(config)

  tasks.keys.sort.each { |task_name| builder.send(task_name[3..-1]) if Tasks[task_name] }

  STDERR.puts('Finished at ' + `date`)
end
