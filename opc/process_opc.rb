require_relative '../shared/process_corpus'

Tasks = {
    '01_init_text_items'  => true,
    '02_create_cwb_files' => true,
    '03_create_explicit_alignment_files' => true,
    '04_register_positions_and_create_tab_files' => true,
    '05_create_alg_files' => true,
    '06_cwb_encode' => true
  #'01_extract_alignments'                      => false,
  #'02_store_metadata_x'                        => true,
  #'03_tag_files'                               => true,
  #'05_create_alg_files'                        => true,
  #'06_cwb_encode'                              => true
}

process_corpus(Tasks, config_fn: File.absolute_path('config.json'))
