#!/bin/sh

MTAG_CMD=$1
VISLCG3_CMD=$2
OBTSTAT_CMD=$3
GRAMMAR=$4

${MTAG_CMD} | ${VISLCG3_CMD} -C latin1 --codepage-input \
  utf-8 -g ${GRAMMAR} --codepage-output utf-8 --no-pass-origin -e | \
  ${OBTSTAT_CMD} | perl -ne 'print if /\S/'
