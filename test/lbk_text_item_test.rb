# encoding: utf-8

require 'test/unit'

require_relative '../shared/lib/lbk_text_item'
require_relative '../shared/lib/globals'
require_relative '../shared/lib/corpus_builder'

class LbkTextItemTest < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    # Do nothing
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  def test_lbk_test_item
    config = read_config_file(File.join(get_root_path, 'lbk', 'config.json'))
    item = LbkTextItem.new(config, path: get_root_path,
                           relative_fn: 'test_area/corpora/lbk/Periodika/AV04/AV04Fa9608.tei')

    assert_not_nil(item.metadata)
    assert_not_nil(item.metadata[:catRef])
    assert_equal(['AV04 HEL04 SPO02'], item.metadata[:catRef])
    assert_not_nil(item.category)
    assert_not_nil(item.topics)
    assert_equal('AV04', item.category)

    assert_equal(2, item.topics.length)
    assert_equal('HEL04', item.topics[0])
    assert_equal('SPO02', item.topics[1])
  end

  def test_lbk_create_vert_file
    config = read_config_file(File.join(get_root_path, 'lbk', 'config.json'))
    item = LbkTextItem.new(config, path: get_root_path,
                           relative_fn: 'test_area/corpora/lbk/Periodika/AV04/AV04Fa9608.tei')

    assert_not_nil(item)
    out = StringIO.new
    item.create_vert_file(out)
    assert_equal("<text>\n<s>\nTorkill\tTorkill\tsubst prop\nfikk\tfå\tverb pre", out.string[0,54])
  end
end