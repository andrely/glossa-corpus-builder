require 'test/unit'

require 'textlabnlp/tree_tagger_config'
require 'textlabnlp/oslo_bergen_tagger'

require_relative '../shared/lib/opc_text_item'
require_relative '../shared/lib/globals'

class OPCTextItemTest < Test::Unit::TestCase
  def test_opc_text_item
    omit("Deprecated")
    item = OPCTextItem.new(nil, path: File.join(get_root_path, 'test_area/corpora/opc/'),
                           relative_fn: 'corpus-text/berlinerpoplene/')
    assert_not_nil(item)
    assert_equal('berlinerpoplene', item.id)
    assert_not_nil(item.parallels)
    assert_kind_of(Enumerable, item.parallels)
    assert_equal(2, item.parallels.count)
    item.parallels.each do |p|
      assert_kind_of(ParallelTextItem, p)
    end
    assert_equal('berlinerpoplene', item.id)
    assert_equal('corpus-text', item.relative_path)
    assert_equal('corpus-text/berlinerpoplene/', item.relative_fn)

    sorted_parallels = item.parallels.sort_by { |p| p.lang }
    assert_equal('berlinerpoplene-nob', sorted_parallels[0].id)
    assert_equal('berlinerpoplene-swe', sorted_parallels[1].id)
    assert_equal(:nob, sorted_parallels[0].lang)
    assert_equal(:swe, sorted_parallels[1].lang)
    assert_equal(:swe, sorted_parallels[0].corresp_lang)
    assert_equal(:nob, sorted_parallels[1].corresp_lang)
    assert_equal("corpus-text/berlinerpoplene/berlinerpoplene-nob.vert",
                 sorted_parallels[0].vert_fn)
    assert_equal("corpus-text/berlinerpoplene/berlinerpoplene-swe.vert",
                 sorted_parallels[1].vert_fn)

    nob_metadata = sorted_parallels[0].metadata
    assert_not_nil(nob_metadata)
    assert_kind_of(Hash, nob_metadata)
    assert_equal(8, nob_metadata.keys.count)
    assert_equal("Berlinerpoplene", nob_metadata[:title])
    assert_equal("Anne B. Ragde", nob_metadata[:author])
    assert_equal(2004, nob_metadata[:pubdate])
    assert_equal(["berlinerpoplene-swe"], nob_metadata[:translations])
    assert_equal(false, nob_metadata[:is_translation])
    assert_equal(nil, nob_metadata[:translator])
    assert_equal("nob", nob_metadata[:lang])
    assert_equal(nil, nob_metadata[:orig_lang])
  end

  def test_opc_create_vert_file
    omit("Deprecated")
    omit_unless((TextlabNLP::OsloBergenTagger.new(config: get_tools_config[:obttagger]).available? and
        TextlabNLP::TreeTaggerConfig.lang_available?(:fra, config: get_tools_config[:treetagger]) and
        TextlabNLP::TreeTaggerConfig.lang_available?(:swe, config: get_tools_config[:treetagger])))
    item = OPCTextItem.new(nil, path: File.join(get_root_path, 'test_area/corpora/opc/'),
                           relative_fn: 'corpus-text/berlinerpoplene/')
    assert_not_nil(item)
    sorted_parallels = item.parallels.sort_by { |p| p.lang }
    nob_item = sorted_parallels[0]
    assert(nob_item)
    assert_equal(:nob, nob_item.lang)

    out = StringIO.new
    nob_item.create_vert_file(out)
    assert_equal("<text>\n<s id=\"berlinerpoplene-nob.s0\">\nBerlinerpoplene\tBerlinerpoplene\tsubst prop\n", out.string[0, 82])

    swe_item = sorted_parallels[1]
    assert(swe_item)
    assert_equal(:swe, swe_item.lang)

    out = StringIO.new
    swe_item.create_vert_file(out)
    assert_equal("<text>\n<s id=\"berlinerpoplene-swe.s0\">\nBerlinerpopplarna\t<unknown>\tNCUPN@DS\n", out.string[0, 76])

    item = OPCTextItem.new(nil, path: File.join(get_root_path, 'test_area/corpora/opc/'),
                           relative_fn: 'corpus-text/en-kassadames-betroelser/')
    assert_not_nil(item)
    sorted_parallels = item.parallels.sort_by { |p| p.lang }
    fre_item = sorted_parallels[0]
    assert(fre_item)
    assert_equal(:fra, fre_item.lang)

    out = StringIO.new
    fre_item.create_vert_file(out)
    assert_equal("<text>\n<s id=\"en-kassadames-betroelser-fre.s0\">\nLes\tle\tDET:ART\n", out.string[0, 63])
  end

  def test_tei_to_text
    tei_file = StringIO.new("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?><TEI.2>\n<teiHeader>\n</teiHeader>\n<text>\n<div>\n<s id=\"1\">Ba.</s>\n<s id=\"2\">Foo.</s>\n</div>\n</text>\n</TEI.2>\n")
    out_file = StringIO.new
    ParallelTextItem.tei_to_text(tei_file, out_file)
    assert_equal("<s id=\"1\">Ba.</s>\n<s id=\"2\">Foo.</s>\n", out_file.string)
  end

  def test_text_to_vert
    text = [{ id: 1,
              words: [{ word: "Hallo",
                        form: "hallo",
                        annotation: [{ tag: "interj", lemma: "hallo"}]},
                      { word: ".",
                        form: ".",
                        annotation: [{ tag: "<punkt>", lemma: "$."}]}]},
            { id: 2,
              words: [{ word: "Vi",
                        form: "vi",
                        annotation: [{ tag: "pron pers", lemma: "vi"}]},
                      { word: "drar",
                        form: "drar",
                        annotation: [{ tag: "verb pres", lemma: "dra" }]},
                      { word: ".",
                        form: ".",
                        annotation: [{ tag: "<punkt>", lemma: "$."}]}]}]
    out_file = StringIO.new
    ParallelTextItem.text_to_vert(out_file, text)
    assert_equal("<text>\n<s id=\"1\">\nHallo\thallo\tinterj\n.\t$.\t<punkt>\n</s>\n<s id=\"2\">\nVi\tvi\tpron pers\ndrar\tdra\tverb pres\n.\t$.\t<punkt>\n</s>\n</text>",
                 out_file.string.strip)
  end

  def test_extract_alignments
    tei_file = StringIO.new("<?xml version=\"1.0\" encoding=\"utf-8\" standalone=\"no\"?><TEI.2>\n<teiHeader>\n</teiHeader>\n<text>\n<div>\n<s id=\"1\" corresp=\"1\">Ba.</s>\n<s id=\"2\" corresp=\"2 3\">Foo.</s>\n</div>\n</text>\n</TEI.2>\n")
    alignments = ParallelTextItem.extract_alignments(tei_file)
    assert(alignments)
    assert_kind_of(Hash, alignments)
    assert_equal(["1", "2"], alignments.keys.sort)
    assert_equal(["1"], alignments["1"])
    assert_equal(["2", "3"], alignments["2"])
  end

  def test_create_alignment_file
    omit("deprecated")
    omit_unless((TextlabNLP::OsloBergenTagger.new(config: get_tools_config[:obttagger]).available? and
        TextlabNLP::TreeTaggerConfig.lang_available?(:swe, config: get_tools_config[:treetagger])))
    item = OPCTextItem.new(nil, path: File.join(get_root_path, 'test_area/corpora/opc/'),
                           relative_fn: 'corpus-text/berlinerpoplene/')
    assert_not_nil(item)
    sorted_parallels = item.parallels.sort_by { |p| p.lang }
    swe_item = sorted_parallels[1]
    assert(swe_item)
    assert_equal(:swe, swe_item.lang)

    out = StringIO.new
    swe_item.create_alignment_file(out)
    assert_equal("berlinerpoplene-swe.s0\tberlinerpoplene-nob.s0\tnob\nberlinerpoplene-swe.s1\tberlinerpoplene-nob.s1\tnob\nberlinerpoplene-swe.s2\tberlinerpoplene-nob.s2\tnob\nberlinerpoplene-swe.s2\tberlinerpoplene-nob.s3\tnob\nberlinerpoplene-swe.s3\tberlinerpoplene-nob.s4\tnob\nberlinerpoplene-swe.s4\tberlinerpoplene-nob.s5\tnob\nberlinerpoplene-swe.s5\tberlinerpoplene-nob.s6\tnob\nberlinerpoplene-swe.s6\tberlinerpoplene-nob.s7\tnob\nberlinerpoplene-swe.s7\tberlinerpoplene-nob.s8\tnob", out.string.strip)
  end

  def test_register_positions
    input = StringIO.new("<text>\n<s id=\"1\">\nBa\n.\n</s>\n<s id=\"2\">\nFoo\nknark\n.\n</s>\n</text>\n")
    output = StringIO.new
    pos = ParallelTextItem.register_positions(output, input, 0)
    # these values must be verified
    assert_equal(5, pos)
    assert_equal("1\t0\t1\n2\t2\t4", output.string.strip)

    input = StringIO.new("<text>\n<s id=\"1\">\nBa\n.\n</s>\n<s id=\"2\">\nFoo\nknark\n.\n</s>\n<s id=\"3\">\nBa\n.\n</s>\n</text>\n")
    output = StringIO.new
    pos = ParallelTextItem.register_positions(output, input, 0)
    # these values must be verified
    assert_equal(7, pos)
    assert_equal("1\t0\t1\n2\t2\t4\n3\t5\t6", output.string.strip)
  end

  def test_extract_lang
    assert_equal([:swe, :nob], OPCTextItem.extract_lang('berlinerpoplene-swe-nob.xml', 'berlinerpoplene'))
  end

  def test_correspondences
    corpus_path = File.join(get_root_path, 'test_area', 'corpora', 'opc', 'corpus-text')
    assert_equal([[:nob, :swe]],
                 OPCTextItem.correspondences('berlinerpoplene', :nob,
                                             File.join(corpus_path, 'berlinerpoplene')))
    assert_equal([[:nob, :fra]],
                 OPCTextItem.correspondences('en-kassadames-betroelser', :nob,
                                             File.join(corpus_path, 'en-kassadames-betroelser')))
    assert_equal([[:nob, :eng], [:nob, :nno]],
                 OPCTextItem.correspondences('udhr', :nob,
                                             File.join(corpus_path, 'udhr')).sort)
  end
end