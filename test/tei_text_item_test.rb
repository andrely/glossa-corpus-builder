require 'test/unit'
require 'cgi'

require_relative '../shared/lib/tei_text_item'
require_relative '../shared/lib/globals'
require_relative '../shared/lib/corpus_builder'

class TeiTextItemTest < Test::Unit::TestCase
  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    # Do nothing
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  def test_tei_text_item
    config = read_config_file(File.join(get_root_path, 'lbk', 'config.json'))
    item = TeiTextItem.new(config, path: get_root_path,
                           relative_fn: 'test_area/corpora/lbk/Periodika/AV04/AV04Fa9608.tei')

    text = File.read(File.join(get_root_path, 'test_area/corpora/lbk/Periodika/AV04/AV04Fa9608.txt'))

    assert_not_nil(item)
    assert_equal('AV04Fa9608', item.id)
    assert_not_nil(item.extract_text)
    assert_kind_of(String, item.extract_text)
    assert_equal(text, item.extract_text)

    # Check metadata
    assert_equal('Torkill fikk en start i motvind - maskinleselig versjon', item.metadata[:fileDesc_title][0])
    # assert_equal('1547', item.metadata[:wordcount])
    assert_equal('Leksikografikorpuset, Institutt for lingvistiske og nordiske studier, Universitetet i Oslo', item.metadata[:fileDesc_distributor][0])
    # assert_equal('', item.metadata[:fileDesc_address])
    assert_equal('2000-07', item.metadata[:fileDesc_date][0])

    assert_equal('Torkill fikk en start i motvind', item.metadata[:analytic_title][0])
    assert_equal('Dehli, Erna', item.metadata[:analytic_authorname][0])
    assert_equal('Dehli, Toralf', item.metadata[:analytic_authorname][1])

    assert_equal('F', item.metadata[:analytic_authorsex][0])
    assert_equal('M', item.metadata[:analytic_authorsex][1])

    assert_equal('', item.metadata[:analytic_authorgeogr][0])
    assert_equal('A03', item.metadata[:analytic_authorgeogr][1])

    assert_equal('1943', item.metadata[:analytic_authorborn][0])
    assert_equal('1942', item.metadata[:analytic_authorborn][1])

    # assert_equal('', item.metadata[:analytic_translator])

    assert_equal('Familien', item.metadata[:monogr_title][0])
    #assert_equal('', item.metadata[:monogr_authorname])
    #assert_equal('', item.metadata[:monogr_authorsex])
    #assert_equal('', item.metadata[:monogr_authorgeogr])
    #assert_equal('', item.metadata[:monogr_authorborn])
    #assert_equal('', item.metadata[:monogr_translator])
    #assert_equal('', item.metadata[:monogr_edition])

    assert_equal('Oslo', item.metadata[:imprint_pubplace][0])
    assert_equal('Hjemmet Mortensen AS', item.metadata[:imprint_publisher][0])
    assert_equal('1996', item.metadata[:imprint_date][0])

    #assert_equal('', item.metadata[:idno][0])
    #assert_equal('', item.metadata[:note][0])
    assert_equal('AV04 HEL04 SPO02', item.metadata[:catRef][0])
    assert_equal('AV04', item.metadata[:catRef][0][0...4])


    assert_equal('29.07.2005', item.metadata[:revision_date][0])
    #assert_equal('', item.metadata[:revision_name][0])
    #assert_equal('', item.metadata[:revision_desc][0])
  end
end