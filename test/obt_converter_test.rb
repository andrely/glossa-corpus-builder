require "test/unit"
require "json"

require_relative "../shared/lib/obt_converter"
require_relative "../shared/lib/globals"

class ObtConverterTest < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    # Do nothing
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  def test_run_obt_converter
    conf = JSON(open(File.join(get_root_path, "lbk/config.json")).readlines().join('')) # .readlines.join(''))) # JSON(open(File.join(get_root_path, "lbk/config.json").readlines.join('')))
    mapping = JSON(open(File.join(get_root_path, "test/data/obt_lbk_mapping.json")).readlines().join(''))
    columns = conf['cwb']['p_attributes']
    input_fn = File.join(get_root_path, "test/data/AV06Test01.txt.okl")
    expected = open(File.join(get_root_path, "test/data/AV06Test01.txt.tab")).readlines.join('')
    output = run_obt_converter_on_file(input_fn, columns, mapping)

    assert_equal(expected.strip, output.strip)
  end
end