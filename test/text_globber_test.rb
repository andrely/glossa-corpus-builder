require 'test/unit'

require_relative '../shared/lib/text_globber'
require_relative '../shared/lib/globals'

class TextGlobberTest < Test::Unit::TestCase
  def test_mini_lbk
    globber = TextGlobber.new(path: File.join(get_root_path, "test_area/corpora/lbk"),
                              pattern: "*/*/*tei")
    assert_not_nil(globber)
    fns = globber.process
    assert_not_nil(fns)
    assert_equal(34, fns.count)
    fns.each { |fn| assert_kind_of(String, fn)}
  end

  def test_brown
    globber = TextGlobber.new(path: File.join(get_root_path, "test_area/corpora/brown"),
                              pattern: "c???")
    assert_not_nil(globber)
    fns = globber.process
    assert_not_nil(fns)
    assert_equal(5, fns.count)
    fns.each { |fn| assert_kind_of(String, fn) }
  end

  def test_opc
    globber = TextGlobber.new(path: File.join(get_root_path, "test_area/corpora/opc"),
                              pattern: "corpus-text/*")
    assert_not_nil(globber)
    fns = globber.process
    assert_not_nil(fns)
    assert_equal(2, fns.count)
    fns.each { |fn| assert_kind_of(String, fn) }
  end

  def test_norm
    globber = TextGlobber.new(path: File.join(get_root_path, "test_area/corpora/norm"),
                              pattern: "import/*_norm.txt")
    assert_not_nil(globber)
    fns = globber.process
    assert_not_nil(fns)
    assert_equal(3, fns.count)
    fns.each { |fn| assert_kind_of(String, fn) }
  end
end
