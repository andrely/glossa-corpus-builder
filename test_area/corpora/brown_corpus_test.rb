require 'test/unit'

require_relative '../../test/test_helper'
require_relative '../../brown/process_brown'

class BrownCorpusTest < Test::Unit::TestCase

  # Called before every test method runs. Can be used
  # to set up fixture information.
  def setup
    # Do nothing
  end

  # Called after every test method runs. Can be used to tear
  # down fixture information.

  def teardown
    # Do nothing
  end

  def test_brown_build
    corpus_path = File.join(File.dirname(__FILE__), 'brown')

    process_brown(path: corpus_path)

    assert(File.exists?(File.join(corpus_path, 'corpus.tab')))
    assert(File.exists?(File.join(corpus_path, 'brown.db')))
    assert(File.exists?(File.join(corpus_path, 'reg', 'brown')))
    assert(File.exists?(File.join(corpus_path, 'dat', 'BROWN', 'word.lexicon')))
    assert(File.exists?(File.join(corpus_path, 'dat', 'BROWN', 'pos.lexicon')))
  end
end
