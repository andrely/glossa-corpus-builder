<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE TEI SYSTEM "http://www.tei-c.org/release/xml/tei/custom/schema/dtd/tei_lite.dtd">
<TEI xmlns="http://www.tei-c.org/ns/1.0" id="SK04RaaL01">
<teiHeader type="text">
<fileDesc>
<titleStmt>
<title>
Hvalfall - maskinleselig versjon</title>
</titleStmt>
<extent bytecount="" wordcount="1737">
</extent>
<publicationStmt>
<distributor>Leksikografikorpuset, Institutt for lingvistiske og nordiske studier, Universitetet i Oslo</distributor>
<address>
<addrLine>Postboks 1102 Blindern, 0317 Oslo</addrLine>
</address>
<date>2011-03</date>
</publicationStmt>
<sourceDesc>
<biblStruct>
<analytic>
<author born="" geogr="" sex=""/>
<title/>
</analytic>
<monogr>
<author born="1988" geogr="A01" sex="M">Raavand, Lars Haga</author>
<title>Hvalfall</title>
<edition n=""/>
<imprint>
<pubPlace>Oslo</pubPlace>
<publisher>Cappelen Damm</publisher>
<date>2011</date>
</imprint>
</monogr>
<idno type="ISBN">9788202349844</idno>
<note/>
</biblStruct>
</sourceDesc>
</fileDesc>
<profileDesc>
<langUsage>
<language>BOKM</language>
</langUsage>
<textClass>
<catRef target="SK04"/>
</textClass>
</profileDesc>
<revisionDesc>
<change>
<date/>
<respStmt>
<name/>
</respStmt>
<item/>
</change>
</revisionDesc>
</teiHeader>
<text>Hvalfall

	Gud bleket de lekende stjerner
i jordens underbevissthet.

Langt under meg ser jeg armene. Vasstrukne, gjennomr�tne armer som vinker. 

Er jeg keiser Nero, kaptein Nemo? Akab?
Sj�gresset b�lger over sletta og det hvite uhyret forsvinner i m�rket. Jeg brenner meg selv i havets velde.

Havet deler seg i lyse strenger. Etterd�nningene. Brenningslagene.
V�re forfedres sang. Det kan ikke v�re noe annet. Vi kom fra havet. Fra det
bl�gr�nne dypet. V�r mor og far i havet, algene, de nesten gule tarebeltene, som sola trenger igjennom.

Landskapet er ogs� inne i meg. Jeg er en landskapsfantasi. En fastlandsforbindelse. 

Skarlagensr�de plankton 
kleber seg til huden.

Fallet i gr�nneste vann er en sti; et �letrangt hav.

Herre, h�r v�r b�nn,
renn dine t�rer p� oss,
vi er i n�d!
Er Himmelens flyvefisk,
lengter etter � ro oss skrikende 
fram gjennom regn.
Vi v�ker, er 
fugler p� taket av drivhuset.
Herre, h�r v�r b�nn,
v�re dager svinner som r�yk,
v�re lemmer brenner som ild.
V�re hjerter er sl�tt og visnet som gress,
for vi har ikke tilgitt at du drev oss ut.
Vi sukker og st�nner,
vi er bare skinn og ben.
Vi ligner pelikanen i �demarken,
er blitt et nebbete kor i myra.
Vi er de d�des skumringsland.
Vi er hundene du g�r i p� den ytterste dag.

� fylle �ynene med glitrende latter n�r kadavarets veldige lemmer �penbarer seg. Det er den st�rste av alle skapninger; sakte synkende. Buken beveger seg ikke lenger rytmisk, bryter dypet og baner vei for resten av hvalen. En sluknet stjerne som snart er sentrum i et fremmed univers: det balaenopteridaiske. Stjernest�vet flykter i universets f�dsel; is�nderrevne koraller; bagateller i m�rkets dyp; partikler av liv; det store smellet; langsomt, langsomt.

Forbrenningen p�g�r konstant, rykkvis. Spekklaget; digre tjafser, lange remser. Energien omformes. Nytt liv etser det gamle vekk; snart ligger kjevebeina nakne; en hvit dverg sammenkr�kt p� havbunnen. � sette seg ved dvergens side, suge beinmargen ut. Se arkivene l�se seg opp, danse som sild, som s�lv.

Herre, vrakfiskene eter av blygheten v�r.
	Gi oss mot!
	For som dagene yngler gj�r ogs� b�rstemarken,
	graver seg ned i dr�mmene v�re. Ubeskjedent.
	Herre, vekk oss!
	Alt det som finnes i s�vnen og i vannet, 
	alt det som vrimler i vannet og i s�vnen, 
	skal v�re noe vi saumfarer, fort�rer.
	For bare slik kan vi verges mot skyt�rst, styrtsj�.
	Herre, hvalskr�mtene brenner i v�rt indre.
	Gi oss en �pning!
	For lyset er ditt,
	som natten og havet
i evighet.

Hvalskjelettet kles nakent, skyves ned i avgrunnens glemsel. Kampen om kadaveret er ikke over. Jeg skal sl�ss. 

Slettene st�r tomme, rundt ribbeina reiser eimen av evig natt et nytt m�rke. Mudderet trekker seg unna. Kun beina er igjen, men jeg er befengt, en �tselmark. Jeg konkurrerer med Osedax om lipidene, borer meg inn i beinmargen og frigir pattedyrets hydrofobi, bare for selv � sluke den. 

Jeg svelger. B�lgespillet; lysrefleksenes skj�nnhet knuses av trykket her nede. Hvalskr�mtene brenner i mitt indre. Jeg skal eksplodere.

� falle til Atollas dyp. Gni verden i �ynene og huke tak i hvalfangstens overlevende; de skal d�. Lysb�lgene g�r i sirkel, Atollamanetens luminescens utl�ses av frykt. D�dskrampene begynner.
Jeg ser en synkende gigant; flere; flere. Medusas r�dhet fester seg i �ynene mine. Den bl� kuppelen p� oversiden av tentaklene stirrer p� meg og historien gjentar seg. Jeg ser havets lys; klingende ild.

� dykke etter Rigels gr�nne enger, bade i den stjernel�se natten. Her er duggen tett og salt som blod. Jernsmaken ruster. Cetacea har ligget her lenge, et styrtet romskip, falt som enkelte ribbein; sammen med bunnen, forsvunnet i dypet. � vandre i dette monumentale minnet, beundre det ubestemmelige i spermhvalens indre. Leke med tiden som har kneppet den opp, spent av str�mpeb�ndene, sn�rt opp hosene og l�snet alle hekter og maljer som holder dens innerste knokler sammen, stilt fram det n�dvendige offerets ultimate skikkelse. 

Dette kj�ret�yet for mine visjoner, krasjlandet, r�ttent; et knirkende hestelass. Fire voksne kvartaler med d�d, jeg bader i spermasettens indre. Skjult for m�nens lys, i skyggen av meg selv, umulig for gatelyktene � fange og sammenlignet med brosteinen er skatene myke som fl�yel. Jeg klamrer meg fast. Konkyliene puster meg i �ret; havets s�vndryssende silkebris.

Havet er verken stort eller lite.
	Det er et palass for fiskene
	eller en d�d glassmanet for sola.
	Havet er et halssmykke for guder
	eller tykt blod for t�rste.

Duggdr�pen er verken stor eller liten.
Hele m�nen og himmelvelvingen 
tar bolig i duggdr�pen.
Spedbarnsh�nden er for stor,
gressets dugg en lukket d�r.

Havet er verken stort eller lite.
Havet er hendene som holder kystene sammen
eller speilet som skiller liv fra d�d.

� se det bunnl�se hierarkiet i �ynene; fra sola treffer m�kevingene; gr�hvalens bl�st lyder hult; fontena glitrer og hvalen vender seg vekk; velter seg vei gjennom vannmassene; helvetesheisen, ned i evig skumring, evig natt. Ribbemanetene reiser seg, mot lyset, flykter.

� v�re en tykkfj�ret kildeorm. Bakteriene tok seg inn i meg mens jeg enn� var ung, gjennom den primitive munnen og magen. Jeg modnes, som morellene i h�stsola, men her finnes ingen sol, bare marin sn�, evig natt. Magen og munnen forsvinner, bakteriene blir v�rende igjen inne i kroppshulen, yngler.

H�ret er b�rstet av fullm�nens blanke skalle, og i m�tet mellom land og vann vaker tangens r�tne lukt. Vi stiller dr�mmene inn mot havenes brus, deres b�lgers brus og urlivets bulder. Tidevannet l�fter sine gjennomsiktige fjell, og lar de atter synke. Som motet i v�re bryst, men ydmykt ber vi deg, Herre, du som alene er alt, du som har gjort himlene, himlenes himler og alle deres h�rskarer, jorden og alt som er p� den, havene og alt som er i dem, og du som holder det alt sammen i live, gi oss sj�sneglenes spor i sikte.

</text>
</TEI>
