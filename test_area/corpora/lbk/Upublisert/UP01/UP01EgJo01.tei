<?xml version="1.0" encoding="iso-8859-1"?>
<!DOCTYPE TEI SYSTEM "http://www.tei-c.org/release/xml/tei/custom/schema/dtd/tei_lite.dtd">
<TEI xmlns="http://www.tei-c.org/ns/1.0" id="UP01EgJo01">
<teiHeader type="text">
<fileDesc>
<titleStmt>
<title>
Sitter som en klegg p� Karzai - maskinleselig versjon</title>
</titleStmt>
<extent bytecount="" wordcount="788">
</extent>
<publicationStmt>
<distributor>Leksikografikorpuset, Institutt for lingvistiske og nordiske studier, Universitetet i Oslo</distributor>
<address>
<addrLine>Postboks 1102 Blindern, 0317 Oslo</addrLine>
</address>
<date>2005-08</date>
</publicationStmt>
<sourceDesc>
<biblStruct>
<analytic>
<author born="1960" geogr="" sex="M">Egen�s, John Peder</author>
<title>Sitter som en klegg p� Karzai</title>
</analytic>
<monogr>
<editor>Tin, Ina</editor>
<title>Amnesty Nytt 1/2003</title>
<edition n=""/>
<imprint>
<pubPlace>Oslo</pubPlace>
<publisher>Amnesty International Norge</publisher>
<date>2003</date>
</imprint>
</monogr>
<idno type=""/>
<note/>
</biblStruct>
</sourceDesc>
</fileDesc>
<profileDesc>
<langUsage>
<language>BOKM</language>
</langUsage>
<textClass>
<catRef target="UP01 SAM05 JUS01"/>
</textClass>
</profileDesc>
<revisionDesc>
<change>
<date/>
<respStmt>
<name/>
</respStmt>
<item/>
</change>
</revisionDesc>
</teiHeader>
<text>'06-JP - Afghanistan-IT'
5800 t.

Sitter som en klegg p� Karzai

- Amnesty fungerer som en klegg her i Kabul. Vi holder menneskerettigheter p� dagsorden, og vi snakker h�yt om ting andre helst vil forbig� i stillhet. 

Av John Peder Egen�s

Det sier Margaret Ladner som leder og koordinerer Amnesty Internationals arbeid i Afghanistan. Organisasjonen �pnet kontor i Kabul i fjor h�st. Hovedoppgavene til kontoret er � gjennomf�re grundige unders�kelser av politi, fengsels- og rettsvesen, utover utallige m�ter med myndigheter og organisasjoner som skal bidra til � sikre menneskerettighetsperspektivet i gjenoppbyggingen av Afghanistan. 

- Som en klegg er vi brysomme. Men kanskje i motsetning til kleggens virksomhet, er v�r helt n�dvendig, mener Margaret Ladner, og forklarer. - Afghanistan er inne i en overgangstid.  Menneskerettighetene kan bygges inn i det nye som vokser fram, eller de kan skyves helt ut over sidelinjen. Valget gj�res n�, og ved � holde menneskerettighetene p� dagsorden kan Amnesty bidra til at overgangen blir til noe bedre for afghanere flest, sier Amnestys kvinne i Kabul.

Det har allerede skjedd noen sv�rt positive forandringer i Afghanistan i l�pet av det siste �ret. Men den generelle usikkerheten er afghanernes st�rste problem for �yeblikket. Institusjonene som er ment � beskytte folk, politi, milit�ret og rettsvesenet er ikke i stand til � levere sikkerhet. Noen steder er det nettopp disse institusjonene som st�r bak overgrepene. 

Det er dette bildet som har skapt Amnestys hovedprioritering: Bidra til at rettsvesenet bygges opp p� en m�te som gj�r det egnet til � beskytte afghanernes menneskerettigheter. Til n� har politi og fengselsvesen blitt lagt under lupen. I n�r framtid vil domstolene og behandlingen av kvinner i rettssystemet generelt bli gransket.

I arbeidet med � kartlegge fengselsforholdene i Afghanistan har Amnesty intervjuet et stort antall menn, kvinner og barn i afghanske fengsler. Etterforskeren fikk deres personlige historier, fortellinger om mishandling og beskrivelser av fengselsforholdene.  De afghanske myndighetene var oppsiktsvekkende samarbeidsvillige. Det var ikke fullt s� enkelt da det var politiets tur. 

- Enkelte politiledere hadde bemerkelsesverdig liten tid, d�rlig helse eller problemer i familien, kommenterer Ladner. - Men mange stilte villig til intervjuer og diskusjon. Samtaler som avsl�rte at det er lang vei � g� f�r Afghanistan har et velfungerende politi. Ansvarlige myndigheter har ogs� vist en enest�ende vilje til � diskutere Amnestys funn og anbefalinger. Margaret Ladner mener det gir h�p om at endring er mulig.

- Et rettsvesen bygd p� menneskerettigheter er avgj�rende for Afghanistans fremtid. Det stiller myndighetene overfor enorme utfordringer. Polititjenestefolk, dommere og fengselsbetjenter m� utdannes og trenes i stor skala. Samtidig st�r man i dag overfor s�pass grunnleggende problemer som at politiet mangler penn og papir mange steder. Hvordan skal de for eksempel kunne registrere arrestanter korrekt da, sp�r Ladner.

Margaret Ladner peker ogs� p� straffefrihet b�de for historiske og n�tidige overgrep, som et stort problem. Erfaring viser at fred og demokrati bygd p� hukommelsestap sjelden varer. Mange hevder at overgrepene som er beg�tt i Afghanistan er s� mange og involverer s� mektige menn, at straffrihet er den eneste l�sningen.

Det er opprettet en afghansk menneskerettighetskommisjon, som blant annet skal takle dette problemet. Den har i sitt mandat � starte en prosess der afghanerne selv skal f� uttrykke sin mening om hva de �nsker skal gj�res med fortidens overgrep og overgripere. Det er essensielt at denne nasjonale dialogen blir igangsatt. For � bekjempe de p�g�ende overgrep trengs et fungerende politi og rettsapparat. 
</text>
</TEI>
