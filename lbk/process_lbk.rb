require_relative '../shared/process_corpus'

Tasks = {
    '01_init_text_items'                               => true,
    '02_store_metadata'                          => false,
    '03_register_positions_and_create_tab_files' => false,
    '04_cwb_encode'                              => false}

process_corpus(Tasks, config_fn: File.absolute_path('config.json'))
