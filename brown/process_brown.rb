require 'optparse'

require_relative '../shared/process_corpus'

# The top level corpus process takes a sequential list pof tasks
# specified in a Hash
Tasks = {"01_store_metadata" => true,
         "02_tag_files" => true,
         "03_cwb_encode" => true,
         "04_generate_config" => true}

DEFAULT_CONFIG_FN = File.join(File.dirname(__FILE__), 'config.json')

DEFAULT_OPTS = { :config_fn => DEFAULT_CONFIG_FN }

def process_brown(opts={})
  process_corpus(Tasks, opts=DEFAULT_OPTS.merge(opts))
end

if __FILE__ == $0
  options = {}

  OptionParser.new do |opts|
    opts.on('-c', '--corpus-config FILE', 'json config file') { |v| options[:config_fn] = v }
    opts.on('-p', '--corpus-path PATH', 'path to corpus data') { |v| options[:path] = v}
  end.parse!

  process_brown(opts=options)
end
